    import javax.swing.*;
    import java.awt.event.ActionEvent;
    import java.awt.event.ActionListener;
    import javax.swing.ImageIcon;

    public class FoodGUI2 {
        private JButton tempuraButton;
        private JButton sobaButton;
        private JButton karaageButton;
        private JButton gyozaButton;
        private JButton hoikoroButton;
        private JButton ramenButton;
        private JPanel root;
        private JButton button7;
        private JTextArea orderedItemsList;
        private JTextPane totalcost;
        int sum = 0;

        public static void main(String[] args) {
            JFrame frame = new JFrame("FoodGUI2");
            frame.setContentPane(new FoodGUI2().root);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setVisible(true);
        }

        void order(String FoodName, int price){
            int confirmation = JOptionPane.showConfirmDialog (null,
                    "Would you like to order " +FoodName+"?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION
            );

            if(confirmation == 0) {
                String currentText = orderedItemsList.getText();
                orderedItemsList.setText(currentText +FoodName+ " " +price+ "yen" + "\n");
                JOptionPane.showMessageDialog(null, "Thank you for Ordering " +FoodName+
                        "! It will be received as soon as possible.");
                sum+=price;
                totalcost.setText("        total         " +sum+ "yen");

            }
        }
        public FoodGUI2() {
            totalcost.setText("        total         " +sum+ "yen");

            tempuraButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Tempura", 150);
                }
            });
            tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("./png/R.jpg")));

            karaageButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Karaage", 200);
                }
            });
            karaageButton.setIcon(new ImageIcon(
                    this.getClass().getResource("./png/2022-11-05.png")));

            gyozaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Gyoza", 250);
                }
            });
            sobaButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Soba", 300);
                }
            });
            hoikoroButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Hoikoro", 350);
                }
            });
            ramenButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Ramen", 400);
                }
            });

            button7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to checkout?",
                            "Order Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    if(confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " +sum+
                                " yen.");
                        sum=0;
                        totalcost.setText("        total         " +sum+ "yen");
                        orderedItemsList.setText(null);
                    }
                }
            });
        }

        private void createUIComponents() {
            // TODO: place custom component creation code here
        }
    }
