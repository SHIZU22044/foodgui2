import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI_Re {
    private JPanel root;
    private JButton TempuraButton;
    private JButton SobaButton;
    private JButton KaraageButton;
    private JButton HoikoroButton;
    private JButton GyozaButton;
    private JButton RamenButton;
    private JButton checkOutButton;
    private JTextPane OrderedItemsList;
    private JLabel TotalCost;
    private JRadioButton takeOutRadioButton;
    private JRadioButton eatInRadioButton;
    private JPanel TaxSelect;
    int sum = 0;

    void order(String FoodName, int price){
        int confirmation = JOptionPane.showConfirmDialog (null,
                "Would you like to order " +FoodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0) {
            String currentText = OrderedItemsList.getText();
            OrderedItemsList.setText(currentText +FoodName+ " " +price+ "yen" + "\n");
            JOptionPane.showMessageDialog(null, "Thank you for Ordering " +FoodName+
                    "! It will be received as soon as possible.");
            sum+=price;
            if(takeOutRadioButton.isSelected()){
                TotalCost.setText((int)(sum*1.08)+ " yen");
            }else if(eatInRadioButton.isSelected()){
                TotalCost.setText((int)(sum*1.10)+ " yen");
            }else{
                TotalCost.setText(sum+ " yen");
            }

        }
    }
    public FoodGUI_Re() {
        TempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 150);
            }
        });
        TempuraButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Tempura.png")));

        KaraageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 200);
            }
        });
        KaraageButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Karaage.png")));

        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 250);
            }
        });
        GyozaButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Gyoza.png")));

        SobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba", 300);
            }
        });
        SobaButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Soba.png")));

        HoikoroButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hoikoro", 350);
            }
        });
        HoikoroButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Hoikoro.png")));

        RamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 400);
            }
        });
        RamenButton.setIcon(new ImageIcon(this.getClass().getResource("./png/Ramen.png")));

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation == 0) {
                    if(takeOutRadioButton.isSelected()){
                        sum = (int)(sum*1.08);
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " +sum+
                                " yen.");
                        sum = 0;
                        TotalCost.setText(sum+ " yen");
                        OrderedItemsList.setText(null);
                    }else if(eatInRadioButton.isSelected()){
                        sum = (int)(sum*1.10);
                        JOptionPane.showMessageDialog(null, "Thank you. The total price is " +sum+
                                " yen.");
                        sum = 0;
                        TotalCost.setText(sum+ " yen");
                        OrderedItemsList.setText(null);
                    }else{
                        JOptionPane.showMessageDialog(null, "You need to choose [Take out] or [Eat in].");
                    }
                }
            }
        });
        takeOutRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TotalCost.setText((int)(sum*1.08)+ " yen");
            }
        });
        eatInRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TotalCost.setText((int)(sum*1.10)+ " yen");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI_Re");
        frame.setContentPane(new FoodGUI_Re().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
